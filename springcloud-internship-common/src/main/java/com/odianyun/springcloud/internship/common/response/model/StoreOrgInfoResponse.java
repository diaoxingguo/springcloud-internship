package com.odianyun.springcloud.internship.common.response.model;

import com.odianyun.project.support.base.model.BaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

/**
 * @description:
 * @author: EDZ
 * @time: 10:14
 * @date: 2021/8/3
 */
@ApiModel(description = "StoreOrgInfoResponse")
public class StoreOrgInfoResponse extends BaseVO {
    /**
     * 组织id
     */
    @ApiModelProperty(name = "orgId", value = "组织id", example = "1")
    private Long orgId;
    /**
     * 组织编码
     */
    @ApiModelProperty(name = "orgCode", value = "组织编码", example = "str")
    private String orgCode;
    /**
     * 所属商家id
     */
    @ApiModelProperty(name = "merchantId", value = "所属商家id", example = "1")
    private Long merchantId;
    /**
     * 审核状态0待审核,1通过,2不通过
     */
    @ApiModelProperty(name = "auditStatus", value = "审核状态0待审核,1通过,2不通过", example = "str")
    private String auditStatus;
    /**
     * 核算单位id
     */
    @ApiModelProperty(name = "accountingUnitId", value = "核算单位id", example = "1")
    private Long accountingUnitId;
    /**
     * 门店类型
     */
    @ApiModelProperty(name = "storeType", value = "门店类型", example = "str")
    private String storeType;
    /**
     * 店铺logo
     */
    @ApiModelProperty(name = "logoUrl", value = "店铺logo", example = "str")
    private String logoUrl;
    /**
     * 微信二维码链接
     */
    @ApiModelProperty(name = "weChatQrCodeUrl", value = "微信二维码链接", example = "str")
    private String weChatQrCodeUrl;
    /**
     * 租金
     */
    @ApiModelProperty(name = "rent", value = "租金", example = "1")
    private BigDecimal rent;
    /**
     * 员工数
     */
    @ApiModelProperty(name = "staffNums", value = "员工数", example = "1")
    private Integer staffNums;
    /**
     * 国家
     */
    @ApiModelProperty(name = "countryCode", value = "国家", example = "str")
    private String countryCode;
    /**
     * 国家名称
     */
    @ApiModelProperty(name = "countryName", value = "国家名称", example = "str")
    private String countryName;
    /**
     * 省id
     */
    @ApiModelProperty(name = "provinceCode", value = "省id", example = "1")
    private Long provinceCode;
    /**
     * 省名
     */
    @ApiModelProperty(name = "provinceName", value = "省名", example = "str")
    private String provinceName;
    /**
     * 市id
     */
    @ApiModelProperty(name = "cityCode", value = "市id", example = "1")
    private Long cityCode;
    /**
     * 市名
     */
    @ApiModelProperty(name = "cityName", value = "市名", example = "str")
    private String cityName;
    /**
     * 区id
     */
    @ApiModelProperty(name = "regionCode", value = "区id", example = "1")
    private Long regionCode;
    /**
     * 区名
     */
    @ApiModelProperty(name = "regionName", value = "区名", example = "str")
    private String regionName;
    /**
     * 详细地址
     */
    @ApiModelProperty(name = "detailAddress", value = "详细地址", example = "str")
    private String detailAddress;
    /**
     * 门店描述
     */
    @ApiModelProperty(name = "desc", value = "门店描述", example = "str")
    private String desc;
    /**
     * 是否接单（是否营业）1-是
     */
    @ApiModelProperty(name = "businessState", value = "是否接单（是否营业）1-是", example = "1")
    private Integer businessState;
    /**
     * 营业执照url
     */
    @ApiModelProperty(name = "businessLicenseUrl", value = "营业执照url", example = "str")
    private String businessLicenseUrl;
    /**
     * 门店招牌url
     */
    @ApiModelProperty(name = "signUrl", value = "门店招牌url", example = "str")
    private String signUrl;
    /**
     * 店铺的经度
     */
    @ApiModelProperty(name = "longitude", value = "店铺的经度", example = "1")
    private BigDecimal longitude;
    /**
     * 店铺的纬度
     */
    @ApiModelProperty(name = "latitude", value = "店铺的纬度", example = "1")
    private BigDecimal latitude;
    /**
     * 服务类型
     */
    @ApiModelProperty(name = "serviceType", value = "服务类型", example = "str")
    private String serviceType;
    /**
     * 客服技能组ID
     */
    @ApiModelProperty(name = "csTekGroupId", value = "客服技能组ID", example = "str")
    private String csTekGroupId;
    /**
     * 是否可用，0-不可用，1可用
     */
    @ApiModelProperty(name = "isAvailable", value = "是否可用，0-不可用，1可用", example = "1")
    private Integer isAvailable;
    /**
     * 详细地址
     */
    @ApiModelProperty(name = "contactDetailAddress", value = "详细地址", example = "str")
    private String contactDetailAddress;
    /**
     * 条款ID
     */
    @ApiModelProperty(name = "entryTermsId", value = "条款ID", example = "1")
    private Long entryTermsId;
    /**
     * 是否允许自创，0不允许，1允许
     */
    @ApiModelProperty(name = "selfCreateCategory", value = "是否允许自创，0不允许，1允许", example = "str")
    private String selfCreateCategory;
    /**
     * 店铺简介
     */
    @ApiModelProperty(name = "shortDesc", value = "店铺简介", example = "str")
    private String shortDesc;
    /**
     * ；联系人email
     */
    @ApiModelProperty(name = "contactEmail", value = "；联系人email", example = "str")
    private String contactEmail;
    /**
     * 线上线下分类0线上1线下
     */
    @ApiModelProperty(name = "storeOnlineType", value = "线上线下分类0线上1线下", example = "str")
    private String storeOnlineType;
    /**
     * 店铺状态0停用，1启用
     */
    @ApiModelProperty(name = "storeStatus", value = "店铺状态0停用，1启用", example = "str")
    private String storeStatus;
    /**
     * 备注
     */
    @ApiModelProperty(name = "contactRemark", value = "备注", example = "str")
    private String contactRemark;
    /**
     * 区名
     */
    @ApiModelProperty(name = "contactRegionName", value = "区名", example = "str")
    private String contactRegionName;
    /**
     * 区code
     */
    @ApiModelProperty(name = "contactRegionCode", value = "区code", example = "1")
    private Long contactRegionCode;
    /**
     * 市名
     */
    @ApiModelProperty(name = "contactCityName", value = "市名", example = "str")
    private String contactCityName;
    /**
     * 市code
     */
    @ApiModelProperty(name = "contactCityCode", value = "市code", example = "1")
    private Long contactCityCode;
    /**
     * 省名
     */
    @ApiModelProperty(name = "contactProvinceName", value = "省名", example = "str")
    private String contactProvinceName;
    /**
     * 省code
     */
    @ApiModelProperty(name = "contactProvinceCode", value = "省code", example = "1")
    private Long contactProvinceCode;
    /**
     * 国家名称
     */
    @ApiModelProperty(name = "contactCountryName", value = "国家名称", example = "str")
    private String contactCountryName;
    /**
     * 国家
     */
    @ApiModelProperty(name = "contactCountryCode", value = "国家", example = "str")
    private String contactCountryCode;
    /**
     * 联系人手机号
     */
    @ApiModelProperty(name = "contactMobileNo", value = "联系人手机号", example = "str")
    private String contactMobileNo;
    /**
     * 联系人
     */
    @ApiModelProperty(name = "contactName", value = "联系人", example = "str")
    private String contactName;
    /**
     * 版本
     */
    @ApiModelProperty(name = "versionNo", value = "版本", example = "1")
    private Integer versionNo;
    /**
     * 创建人IP
     */
    @ApiModelProperty(name = "createUserIp", value = "创建人IP", example = "str")
    private String createUserIp;
    /**
     * 创建人MAC地址
     */
    @ApiModelProperty(name = "createUserMac", value = "创建人MAC地址", example = "str")
    private String createUserMac;
    /**
     * 最后修改人IP
     */
    @ApiModelProperty(name = "updateUserIp", value = "最后修改人IP", example = "str")
    private String updateUserIp;
    /**
     * 最后修改人MAC
     */
    @ApiModelProperty(name = "updateUserMac", value = "最后修改人MAC", example = "str")
    private String updateUserMac;
    /**
     * 库容
     */
    @ApiModelProperty(name = "storageCapacity", value = "库容", example = "1")
    private BigDecimal storageCapacity;
    /**
     * 库容单位(1:M^22:M^33:T4:KG)
     */
    @ApiModelProperty(name = "storageCapacityUnits", value = "库容单位(1:M^22:M^33:T4:KG)", example = "str")
    private String storageCapacityUnits;
    /**
     * 营业面积
     */
    @ApiModelProperty(name = "businessArea", value = "营业面积", example = "1")
    private BigDecimal businessArea;
    /**
     * 营业面积单位(1:M^2)
     */
    @ApiModelProperty(name = "businessAreaUnits", value = "营业面积单位(1:M^2)", example = "str")
    private String businessAreaUnits;
    /**
     * 联系人英文详细地址
     */
    @ApiModelProperty(name = "contactDetailAddressLan2", value = "联系人英文详细地址", example = "str")
    private String contactDetailAddressLan2;
    /**
     * 店铺英文简介
     */
    @ApiModelProperty(name = "shortDescLan2", value = "店铺英文简介", example = "str")
    private String shortDescLan2;
    /**
     * 店铺英文介绍
     */
    @ApiModelProperty(name = "descLan2", value = "店铺英文介绍", example = "str")
    private String descLan2;
    /**
     * 组织英文详细地址
     */
    @ApiModelProperty(name = "detailAddressLan2", value = "组织英文详细地址", example = "str")
    private String detailAddressLan2;
    /**
     * 称重条码开头数字，多个数字用逗号隔开
     */
    @ApiModelProperty(name = "weightNo", value = "称重条码开头数字，多个数字用逗号隔开", example = "str")
    private String weightNo;
    /**
     * 积分定价方式：0:禁用,1:启用
     */
    @ApiModelProperty(name = "pricingMode", value = "积分定价方式：0:禁用,1:启用", example = "1")
    private Integer pricingMode;

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }
    public Long getOrgId() {
        return orgId;
    }
    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }
    public String getOrgCode() {
        return orgCode;
    }
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }
    public Long getMerchantId() {
        return merchantId;
    }
    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }
    public String getAuditStatus() {
        return auditStatus;
    }
    public void setAccountingUnitId(Long accountingUnitId) {
        this.accountingUnitId = accountingUnitId;
    }
    public Long getAccountingUnitId() {
        return accountingUnitId;
    }
    public void setStoreType(String storeType) {
        this.storeType = storeType;
    }
    public String getStoreType() {
        return storeType;
    }
    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }
    public String getLogoUrl() {
        return logoUrl;
    }
    public void setWeChatQrCodeUrl(String weChatQrCodeUrl) {
        this.weChatQrCodeUrl = weChatQrCodeUrl;
    }
    public String getWeChatQrCodeUrl() {
        return weChatQrCodeUrl;
    }
    public void setRent(BigDecimal rent) {
        this.rent = rent;
    }
    public BigDecimal getRent() {
        return rent;
    }
    public void setStaffNums(Integer staffNums) {
        this.staffNums = staffNums;
    }
    public Integer getStaffNums() {
        return staffNums;
    }
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    public String getCountryCode() {
        return countryCode;
    }
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
    public String getCountryName() {
        return countryName;
    }
    public void setProvinceCode(Long provinceCode) {
        this.provinceCode = provinceCode;
    }
    public Long getProvinceCode() {
        return provinceCode;
    }
    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }
    public String getProvinceName() {
        return provinceName;
    }
    public void setCityCode(Long cityCode) {
        this.cityCode = cityCode;
    }
    public Long getCityCode() {
        return cityCode;
    }
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    public String getCityName() {
        return cityName;
    }
    public void setRegionCode(Long regionCode) {
        this.regionCode = regionCode;
    }
    public Long getRegionCode() {
        return regionCode;
    }
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
    public String getRegionName() {
        return regionName;
    }
    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }
    public String getDetailAddress() {
        return detailAddress;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getDesc() {
        return desc;
    }
    public void setBusinessState(Integer businessState) {
        this.businessState = businessState;
    }
    public Integer getBusinessState() {
        return businessState;
    }
    public void setBusinessLicenseUrl(String businessLicenseUrl) {
        this.businessLicenseUrl = businessLicenseUrl;
    }
    public String getBusinessLicenseUrl() {
        return businessLicenseUrl;
    }
    public void setSignUrl(String signUrl) {
        this.signUrl = signUrl;
    }
    public String getSignUrl() {
        return signUrl;
    }
    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }
    public BigDecimal getLongitude() {
        return longitude;
    }
    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }
    public BigDecimal getLatitude() {
        return latitude;
    }
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
    public String getServiceType() {
        return serviceType;
    }
    public void setCsTekGroupId(String csTekGroupId) {
        this.csTekGroupId = csTekGroupId;
    }
    public String getCsTekGroupId() {
        return csTekGroupId;
    }
    public void setIsAvailable(Integer isAvailable) {
        this.isAvailable = isAvailable;
    }
    public Integer getIsAvailable() {
        return isAvailable;
    }
    public void setContactDetailAddress(String contactDetailAddress) {
        this.contactDetailAddress = contactDetailAddress;
    }
    public String getContactDetailAddress() {
        return contactDetailAddress;
    }
    public void setEntryTermsId(Long entryTermsId) {
        this.entryTermsId = entryTermsId;
    }
    public Long getEntryTermsId() {
        return entryTermsId;
    }
    public void setSelfCreateCategory(String selfCreateCategory) {
        this.selfCreateCategory = selfCreateCategory;
    }
    public String getSelfCreateCategory() {
        return selfCreateCategory;
    }
    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }
    public String getShortDesc() {
        return shortDesc;
    }
    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }
    public String getContactEmail() {
        return contactEmail;
    }
    public void setStoreOnlineType(String storeOnlineType) {
        this.storeOnlineType = storeOnlineType;
    }
    public String getStoreOnlineType() {
        return storeOnlineType;
    }
    public void setStoreStatus(String storeStatus) {
        this.storeStatus = storeStatus;
    }
    public String getStoreStatus() {
        return storeStatus;
    }
    public void setContactRemark(String contactRemark) {
        this.contactRemark = contactRemark;
    }
    public String getContactRemark() {
        return contactRemark;
    }
    public void setContactRegionName(String contactRegionName) {
        this.contactRegionName = contactRegionName;
    }
    public String getContactRegionName() {
        return contactRegionName;
    }
    public void setContactRegionCode(Long contactRegionCode) {
        this.contactRegionCode = contactRegionCode;
    }
    public Long getContactRegionCode() {
        return contactRegionCode;
    }
    public void setContactCityName(String contactCityName) {
        this.contactCityName = contactCityName;
    }
    public String getContactCityName() {
        return contactCityName;
    }
    public void setContactCityCode(Long contactCityCode) {
        this.contactCityCode = contactCityCode;
    }
    public Long getContactCityCode() {
        return contactCityCode;
    }
    public void setContactProvinceName(String contactProvinceName) {
        this.contactProvinceName = contactProvinceName;
    }
    public String getContactProvinceName() {
        return contactProvinceName;
    }
    public void setContactProvinceCode(Long contactProvinceCode) {
        this.contactProvinceCode = contactProvinceCode;
    }
    public Long getContactProvinceCode() {
        return contactProvinceCode;
    }
    public void setContactCountryName(String contactCountryName) {
        this.contactCountryName = contactCountryName;
    }
    public String getContactCountryName() {
        return contactCountryName;
    }
    public void setContactCountryCode(String contactCountryCode) {
        this.contactCountryCode = contactCountryCode;
    }
    public String getContactCountryCode() {
        return contactCountryCode;
    }
    public void setContactMobileNo(String contactMobileNo) {
        this.contactMobileNo = contactMobileNo;
    }
    public String getContactMobileNo() {
        return contactMobileNo;
    }
    public void setContactName(String contactName) {
        this.contactName = contactName;
    }
    public String getContactName() {
        return contactName;
    }
    public void setVersionNo(Integer versionNo) {
        this.versionNo = versionNo;
    }
    public Integer getVersionNo() {
        return versionNo;
    }
    public void setCreateUserIp(String createUserIp) {
        this.createUserIp = createUserIp;
    }
    public String getCreateUserIp() {
        return createUserIp;
    }
    public void setCreateUserMac(String createUserMac) {
        this.createUserMac = createUserMac;
    }
    public String getCreateUserMac() {
        return createUserMac;
    }
    public void setUpdateUserIp(String updateUserIp) {
        this.updateUserIp = updateUserIp;
    }
    public String getUpdateUserIp() {
        return updateUserIp;
    }
    public void setUpdateUserMac(String updateUserMac) {
        this.updateUserMac = updateUserMac;
    }
    public String getUpdateUserMac() {
        return updateUserMac;
    }
    public void setStorageCapacity(BigDecimal storageCapacity) {
        this.storageCapacity = storageCapacity;
    }
    public BigDecimal getStorageCapacity() {
        return storageCapacity;
    }
    public void setStorageCapacityUnits(String storageCapacityUnits) {
        this.storageCapacityUnits = storageCapacityUnits;
    }
    public String getStorageCapacityUnits() {
        return storageCapacityUnits;
    }
    public void setBusinessArea(BigDecimal businessArea) {
        this.businessArea = businessArea;
    }
    public BigDecimal getBusinessArea() {
        return businessArea;
    }
    public void setBusinessAreaUnits(String businessAreaUnits) {
        this.businessAreaUnits = businessAreaUnits;
    }
    public String getBusinessAreaUnits() {
        return businessAreaUnits;
    }
    public void setContactDetailAddressLan2(String contactDetailAddressLan2) {
        this.contactDetailAddressLan2 = contactDetailAddressLan2;
    }
    public String getContactDetailAddressLan2() {
        return contactDetailAddressLan2;
    }
    public void setShortDescLan2(String shortDescLan2) {
        this.shortDescLan2 = shortDescLan2;
    }
    public String getShortDescLan2() {
        return shortDescLan2;
    }
    public void setDescLan2(String descLan2) {
        this.descLan2 = descLan2;
    }
    public String getDescLan2() {
        return descLan2;
    }
    public void setDetailAddressLan2(String detailAddressLan2) {
        this.detailAddressLan2 = detailAddressLan2;
    }
    public String getDetailAddressLan2() {
        return detailAddressLan2;
    }
    public void setWeightNo(String weightNo) {
        this.weightNo = weightNo;
    }
    public String getWeightNo() {
        return weightNo;
    }
    public void setPricingMode(Integer pricingMode) {
        this.pricingMode = pricingMode;
    }
    public Integer getPricingMode() {
        return pricingMode;
    }

}
