package com.odianyun.springcloud.internship.oms.mapper;

import com.odianyun.db.mybatis.BaseJdbcMapper;
import com.odianyun.springcloud.internship.oms.model.po.SoPO;

public interface SoMapper extends BaseJdbcMapper<SoPO, Long> {

}