package com.odianyun.springcloud.internship.oms.model.dto;

import com.odianyun.db.mybatis.base.IBaseId;
import com.odianyun.project.base.IEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Date;

/**
 * So
 * @CreateDate 2021-08-02
 */
 @ApiModel("SoDTO")
public class SoDTO implements IEntity, IBaseId<Long> {
    @ApiModelProperty
    private Long id;
    /**
     * 格式：150905xxxxxxxx2657纯数字6位日期+8位数字+1校验位+3位用户id
     */
    @NotNull(message = "格式：150905xxxxxxxx2657纯数字6位日期+8位数字+1校验位+3位用户id{javax.validation.constraints.NotNull.message}")
    @Size(min = 1, max = 20, message = "格式：150905xxxxxxxx2657纯数字6位日期+8位数字+1校验位+3位用户id输入不正确")
    @ApiModelProperty(name = "orderCode", value="格式：150905xxxxxxxx2657纯数字6位日期+8位数字+1校验位+3位用户id", example = "str", required = true, notes="最大长度：20")
    private String orderCode;
    /**
     * 格式同orderCode,复制订单时用来标识复制订单来源
     */
    @Size(min = 0, max = 20, message = "格式同orderCode,复制订单时用来标识复制订单来源输入不正确")
    @ApiModelProperty(name = "copyOrderCode", value="格式同orderCode,复制订单时用来标识复制订单来源", example = "str", notes="最大长度：20")
    private String copyOrderCode;
    /**
     * 父order_code
     */
    @Size(min = 0, max = 20, message = "父order_code输入不正确")
    @ApiModelProperty(name = "parentOrderCode", value="父order_code", example = "str", notes="最大长度：20")
    private String parentOrderCode;
    /**
     * 1子单2父单
     */
    @ApiModelProperty(name = "isLeaf", value="1子单2父单", example = "1", notes="最大长度：10")
    private Integer isLeaf;
    /**
     * 用户ID
     */
    @ApiModelProperty(name = "userId", value="用户ID", example = "1", notes="最大长度：19")
    private Long userId;
    /**
     * 下单用户账号
     */
    @Size(min = 0, max = 50, message = "下单用户账号输入不正确")
    @ApiModelProperty(name = "userName", value="下单用户账号", example = "str", notes="最大长度：50")
    private String userName;
    /**
     * 商家ID
     */
    @ApiModelProperty(name = "merchantId", value="商家ID", example = "1", notes="最大长度：19")
    private Long merchantId;
    /**
     * 经销商id
     */
    @ApiModelProperty(name = "customerId", value="经销商id", example = "1", notes="最大长度：19")
    private Long customerId;
    /**
     * 客户名称
     */
    @Size(min = 0, max = 100, message = "客户名称输入不正确")
    @ApiModelProperty(name = "customerName", value="客户名称", example = "str", notes="最大长度：100")
    private String customerName;
    /**
     * 经销商类型
     */
    @Size(min = 0, max = 10, message = "经销商类型输入不正确")
    @ApiModelProperty(name = "customerType", value="经销商类型", example = "str", notes="最大长度：10")
    private String customerType;
    /**
     * 订单金额(不含运费/运费险)
     */
    @ApiModelProperty(name = "orderAmount", value="订单金额(不含运费/运费险)", example = "1", notes="最大长度：18")
    private BigDecimal orderAmount;
    /**
     * 订单商品总金额
     */
    @ApiModelProperty(name = "productAmount", value="订单商品总金额", example = "1", notes="最大长度：18")
    private BigDecimal productAmount;
    /**
     * 币别码
     */
    @Size(min = 0, max = 10, message = "币别码输入不正确")
    @ApiModelProperty(name = "currency", value="币别码", example = "str", notes="最大长度：10")
    private String currency;
    /**
     * 币别名称
     */
    @Size(min = 0, max = 10, message = "币别名称输入不正确")
    @ApiModelProperty(name = "currencyName", value="币别名称", example = "str", notes="最大长度：10")
    private String currencyName;
    /**
     * 币种汇率
     */
    @ApiModelProperty(name = "currencyRate", value="币种汇率", example = "1", notes="最大长度：10")
    private BigDecimal currencyRate;
    /**
     * 币种符号
     */
    @Size(min = 0, max = 10, message = "币种符号输入不正确")
    @ApiModelProperty(name = "currencySymbol", value="币种符号", example = "str", notes="最大长度：10")
    private String currencySymbol;
    /**
     * 税费
     */
    @ApiModelProperty(name = "taxAmount", value="税费", example = "1", notes="最大长度：18")
    private BigDecimal taxAmount;
    /**
     * 订单状态,字典ORDER_STATUS
     */
    @ApiModelProperty(name = "orderStatus", value="订单状态,字典ORDER_STATUS", example = "1", notes="最大长度：10")
    private Integer orderStatus;
    /**
     * 下单时间
     */
    @ApiModelProperty(name = "orderCreateTime", value="下单时间", example = "2020-05-20", notes="最大长度：19")
    private Date orderCreateTime;
    /**
     * 支付方式,字典PAY_METHOD
     */
    @ApiModelProperty(name = "orderPaymentType", value="支付方式,字典PAY_METHOD", example = "1", notes="最大长度：10")
    private Integer orderPaymentType;
    /**
     * 支付状态,字典ORDER_PAYMENT_STATUS
     */
    @ApiModelProperty(name = "orderPaymentStatus", value="支付状态,字典ORDER_PAYMENT_STATUS", example = "1", notes="最大长度：10")
    private Integer orderPaymentStatus;
    /**
     * 支付确认时间
     */
    @ApiModelProperty(name = "orderPaymentConfirmDate", value="支付确认时间", example = "2020-05-20", notes="最大长度：19")
    private Date orderPaymentConfirmDate;
    /**
     * 运费(实收)
     */
    @ApiModelProperty(name = "orderDeliveryFee", value="运费(实收)", example = "1", notes="最大长度：18")
    private BigDecimal orderDeliveryFee;
    /**
     * 支付-抵用券支付的金额
     */
    @ApiModelProperty(name = "orderPaidByCoupon", value="支付-抵用券支付的金额", example = "1", notes="最大长度：18")
    private BigDecimal orderPaidByCoupon;
    /**
     * 订单已优惠金额(满立减)
     */
    @ApiModelProperty(name = "orderPromotionDiscount", value="订单已优惠金额(满立减)", example = "1", notes="最大长度：18")
    private BigDecimal orderPromotionDiscount;
    /**
     * 订单赠送的积分
     */
    @ApiModelProperty(name = "orderGivePoints", value="订单赠送的积分", example = "1", notes="最大长度：18")
    private BigDecimal orderGivePoints;
    /**
     * 取消原因ID
     */
    @ApiModelProperty(name = "orderCancelReasonId", value="取消原因ID", example = "1", notes="最大长度：10")
    private Integer orderCancelReasonId;
    /**
     * 取消时间
     */
    @ApiModelProperty(name = "orderCancelDate", value="取消时间", example = "2020-05-20", notes="最大长度：19")
    private Date orderCancelDate;
    /**
     * 订单取消原因
     */
    @Size(min = 0, max = 500, message = "订单取消原因输入不正确")
    @ApiModelProperty(name = "orderCsCancelReason", value="订单取消原因", example = "str", notes="最大长度：500")
    private String orderCsCancelReason;
    /**
     * 取消操作人类型：0:用户取消1:系统取消2:客服取消
     */
    @ApiModelProperty(name = "orderCanceOperateType", value="取消操作人类型：0:用户取消1:系统取消2:客服取消", example = "1", notes="最大长度：10")
    private Integer orderCanceOperateType;
    /**
     * 取消操作人用户名
     */
    @Size(min = 0, max = 50, message = "取消操作人用户名输入不正确")
    @ApiModelProperty(name = "orderCanceOperateId", value="取消操作人用户名", example = "str", notes="最大长度：50")
    private String orderCanceOperateId;
    /**
     * 配送方式类型
     */
    @Size(min = 0, max = 20, message = "配送方式类型输入不正确")
    @ApiModelProperty(name = "orderDeliveryMethodId", value="配送方式类型", example = "str", notes="最大长度：20")
    private String orderDeliveryMethodId;
    /**
     * 销售员id
     */
    @ApiModelProperty(name = "salesmanId", value="销售员id", example = "1", notes="最大长度：19")
    private Long salesmanId;
    /**
     * 销售员name
     */
    @Size(min = 0, max = 50, message = "销售员name输入不正确")
    @ApiModelProperty(name = "salesmanName", value="销售员name", example = "str", notes="最大长度：50")
    private String salesmanName;
    /**
     * 订单备注(用户)
     */
    @Size(min = 0, max = 2000, message = "订单备注(用户)输入不正确")
    @ApiModelProperty(name = "orderRemarkUser", value="订单备注(用户)", example = "str", notes="最大长度：2000")
    private String orderRemarkUser;
    /**
     * 订单备注(商家给用户看的)
     */
    @Size(min = 0, max = 2000, message = "订单备注(商家给用户看的)输入不正确")
    @ApiModelProperty(name = "orderRemarkMerchant2user", value="订单备注(商家给用户看的)", example = "str", notes="最大长度：2000")
    private String orderRemarkMerchant2user;
    /**
     * 订单备注(商家自己看的)
     */
    @Size(min = 0, max = 2000, message = "订单备注(商家自己看的)输入不正确")
    @ApiModelProperty(name = "orderRemarkMerchant", value="订单备注(商家自己看的)", example = "str", notes="最大长度：2000")
    private String orderRemarkMerchant;
    /**
     * 订单来源字典ORDER_SOURCE
     */
    @ApiModelProperty(name = "orderSource", value="订单来源字典ORDER_SOURCE", example = "1", notes="最大长度：10")
    private Integer orderSource;
    /**
     * 订单渠道字典ORDER_CHANNEL
     */
    @ApiModelProperty(name = "orderChannel", value="订单渠道字典ORDER_CHANNEL", example = "1", notes="最大长度：10")
    private Integer orderChannel;
    /**
     * 订单促销状态：1001拼团中，1002拼团成功，1003拼团失败，1004参团失败，1006参团成功，1005取消参团；3001待补货，3002、补货中3003补货成功，3004已取消
     */
    @ApiModelProperty(name = "orderPromotionStatus", value="订单促销状态：1001拼团中，1002拼团成功，1003拼团失败，1004参团失败，1006参团成功，1005取消参团；3001待补货，3002、补货中3003补货成功，3004已取消", example = "1", notes="最大长度：10")
    private Integer orderPromotionStatus;
    /**
     * 收货人地址
     */
    @Size(min = 0, max = 400, message = "收货人地址输入不正确")
    @ApiModelProperty(name = "goodReceiverAddress", value="收货人地址", example = "str", notes="最大长度：400")
    private String goodReceiverAddress;
    /**
     * 收货人地址邮编
     */
    @Size(min = 0, max = 10, message = "收货人地址邮编输入不正确")
    @ApiModelProperty(name = "goodReceiverPostcode", value="收货人地址邮编", example = "str", notes="最大长度：10")
    private String goodReceiverPostcode;
    /**
     * 收货人姓名
     */
    @Size(min = 0, max = 100, message = "收货人姓名输入不正确")
    @ApiModelProperty(name = "goodReceiverName", value="收货人姓名", example = "str", notes="最大长度：100")
    private String goodReceiverName;
    /**
     * 收货人手机
     */
    @Size(min = 0, max = 50, message = "收货人手机输入不正确")
    @ApiModelProperty(name = "goodReceiverMobile", value="收货人手机", example = "str", notes="最大长度：50")
    private String goodReceiverMobile;
    /**
     * 收货人国家
     */
    @Size(min = 0, max = 100, message = "收货人国家输入不正确")
    @ApiModelProperty(name = "goodReceiverCountry", value="收货人国家", example = "str", notes="最大长度：100")
    private String goodReceiverCountry;
    /**
     * 收货人省份
     */
    @Size(min = 0, max = 50, message = "收货人省份输入不正确")
    @ApiModelProperty(name = "goodReceiverProvince", value="收货人省份", example = "str", notes="最大长度：50")
    private String goodReceiverProvince;
    /**
     * 收货人城市
     */
    @Size(min = 0, max = 100, message = "收货人城市输入不正确")
    @ApiModelProperty(name = "goodReceiverCity", value="收货人城市", example = "str", notes="最大长度：100")
    private String goodReceiverCity;
    /**
     * 收货人地区
     */
    @Size(min = 0, max = 100, message = "收货人地区输入不正确")
    @ApiModelProperty(name = "goodReceiverCounty", value="收货人地区", example = "str", notes="最大长度：100")
    private String goodReceiverCounty;
    /**
     * 收货人四级区域
     */
    @Size(min = 0, max = 100, message = "收货人四级区域输入不正确")
    @ApiModelProperty(name = "goodReceiverArea", value="收货人四级区域", example = "str", notes="最大长度：100")
    private String goodReceiverArea;
    /**
     * 身份证号码
     */
    @Size(min = 0, max = 40, message = "身份证号码输入不正确")
    @ApiModelProperty(name = "identityCardNumber", value="身份证号码", example = "str", notes="最大长度：40")
    private String identityCardNumber;
    /**
     * 订单出库时间
     */
    @ApiModelProperty(name = "orderLogisticsTime", value="订单出库时间", example = "2020-05-20", notes="最大长度：19")
    private Date orderLogisticsTime;
    /**
     * 订单收货时间
     */
    @ApiModelProperty(name = "orderReceiveDate", value="订单收货时间", example = "2020-05-20", notes="最大长度：19")
    private Date orderReceiveDate;
    /**
     * 0：未删除1：回收站-用户可恢复到02：用户完全删除(客服可协助恢复到0或1)
     */
    @ApiModelProperty(name = "orderDeleteStatus", value="0：未删除1：回收站-用户可恢复到02：用户完全删除(客服可协助恢复到0或1)", example = "1", notes="最大长度：10")
    private Integer orderDeleteStatus;
    /**
     * 改价前订单金额(不含运费/运费险)
     */
    @ApiModelProperty(name = "orderBeforeAmount", value="改价前订单金额(不含运费/运费险)", example = "1", notes="最大长度：18")
    private BigDecimal orderBeforeAmount;
    /**
     * 改价前运费(实收)
     */
    @ApiModelProperty(name = "orderBeforeDeliveryFee", value="改价前运费(实收)", example = "1", notes="最大长度：18")
    private BigDecimal orderBeforeDeliveryFee;
    /**
     * 订单来源系统
     */
    @Size(min = 0, max = 50, message = "订单来源系统输入不正确")
    @ApiModelProperty(name = "sysSource", value="订单来源系统", example = "str", notes="最大长度：50")
    private String sysSource;
    /**
     * 外部系统订单编号
     */
    @Size(min = 0, max = 50, message = "外部系统订单编号输入不正确")
    @ApiModelProperty(name = "outOrderCode", value="外部系统订单编号", example = "str", notes="最大长度：50")
    private String outOrderCode;
    /**
     * 评论状态0:未评论1已评论
     */
    @ApiModelProperty(name = "commentStatus", value="评论状态0:未评论1已评论", example = "1", notes="最大长度：10")
    private Integer commentStatus;
    /**
     * 商家名称
     */
    @Size(min = 0, max = 100, message = "商家名称输入不正确")
    @ApiModelProperty(name = "merchantName", value="商家名称", example = "str", notes="最大长度：100")
    private String merchantName;
    /**
     * 平台备注
     */
    @Size(min = 0, max = 255, message = "平台备注输入不正确")
    @ApiModelProperty(name = "orderRemarkCompany", value="平台备注", example = "str", notes="最大长度：255")
    private String orderRemarkCompany;
    /**
     * 订单完成时间
     */
    @ApiModelProperty(name = "orderCompleteDate", value="订单完成时间", example = "2020-05-20", notes="最大长度：19")
    private Date orderCompleteDate;
    /**
     * 订单类型，对应so_type
     */
    @ApiModelProperty(name = "orderType", value="订单类型，对应so_type", example = "1", notes="最大长度：10")
    private Integer orderType;
    /**
     * 收货人国家code
     */
    @Size(min = 0, max = 64, message = "收货人国家code输入不正确")
    @ApiModelProperty(name = "goodReceiverCountryCode", value="收货人国家code", example = "str", notes="最大长度：64")
    private String goodReceiverCountryCode;
    /**
     * 收货人省份code
     */
    @Size(min = 0, max = 64, message = "收货人省份code输入不正确")
    @ApiModelProperty(name = "goodReceiverProvinceCode", value="收货人省份code", example = "str", notes="最大长度：64")
    private String goodReceiverProvinceCode;
    /**
     * 收货人城市code
     */
    @Size(min = 0, max = 64, message = "收货人城市code输入不正确")
    @ApiModelProperty(name = "goodReceiverCityCode", value="收货人城市code", example = "str", notes="最大长度：64")
    private String goodReceiverCityCode;
    /**
     * 收货人四级区域code
     */
    @Size(min = 0, max = 64, message = "收货人四级区域code输入不正确")
    @ApiModelProperty(name = "goodReceiverAreaCode", value="收货人四级区域code", example = "str", notes="最大长度：64")
    private String goodReceiverAreaCode;
    /**
     * 店铺Id
     */
    @ApiModelProperty(name = "storeId", value="店铺Id", example = "1", notes="最大长度：19")
    private Long storeId;
    /**
     * 店铺名称
     */
    @Size(min = 0, max = 30, message = "店铺名称输入不正确")
    @ApiModelProperty(name = "storeName", value="店铺名称", example = "str", notes="最大长度：30")
    private String storeName;
    /**
     * 订单标签
     */
    @Size(min = 0, max = 2000, message = "订单标签输入不正确")
    @ApiModelProperty(name = "orderLabel", value="订单标签", example = "str", notes="最大长度：2000")
    private String orderLabel;
    /**
     * 预计发货日期
     */
    @ApiModelProperty(name = "expectDeliverDate", value="预计发货日期", example = "2020-05-20", notes="最大长度：10")
    private Date expectDeliverDate;
    /**
     * 收银员-如果是POS渠道的订单，会同步
     */
    @Size(min = 0, max = 100, message = "收银员-如果是POS渠道的订单，会同步输入不正确")
    @ApiModelProperty(name = "cashier", value="收银员-如果是POS渠道的订单，会同步", example = "str", notes="最大长度：100")
    private String cashier;
    /**
     * 团单号
     */
    @Size(min = 0, max = 60, message = "团单号输入不正确")
    @ApiModelProperty(name = "sourceCode", value="团单号", example = "str", notes="最大长度：60")
    private String sourceCode;
    /**
     * 就餐类型1堂吃2外带
     */
    @ApiModelProperty(name = "mealType", value="就餐类型1堂吃2外带", example = "1", notes="最大长度：3")
    private Integer mealType;
    /**
     * 订单设备号
     */
    @Size(min = 0, max = 50, message = "订单设备号输入不正确")
    @ApiModelProperty(name = "equipCode", value="订单设备号", example = "str", notes="最大长度：50")
    private String equipCode;
    /**
     * 桌名
     */
    @Size(min = 0, max = 60, message = "桌名输入不正确")
    @ApiModelProperty(name = "tableName", value="桌名", example = "str", notes="最大长度：60")
    private String tableName;
    /**
     * 就餐人数
     */
    @ApiModelProperty(name = "mealsNum", value="就餐人数", example = "1", notes="最大长度：7")
    private Integer mealsNum;
    /**
     * 订单序号
     */
    @Size(min = 0, max = 20, message = "订单序号输入不正确")
    @ApiModelProperty(name = "seqNo", value="订单序号", example = "str", notes="最大长度：20")
    private String seqNo;
    /**
     * 外部出库单号
     */
    @Size(min = 0, max = 50, message = "外部出库单号输入不正确")
    @ApiModelProperty(name = "outSendCode", value="外部出库单号", example = "str", notes="最大长度：50")
    private String outSendCode;
    /**
     * 扩展信息，以json形式存储
     */
    @Size(min = 0, max = 3000, message = "扩展信息，以json形式存储输入不正确")
    @ApiModelProperty(name = "extInfo", value="扩展信息，以json形式存储", example = "str", notes="最大长度：3000")
    private String extInfo;
    /**
     * 扩展字段1
     */
    @Size(min = 0, max = 200, message = "扩展字段1输入不正确")
    @ApiModelProperty(name = "extField1", value="扩展字段1", example = "str", notes="最大长度：200")
    private String extField1;
    /**
     * 扩展字段2
     */
    @Size(min = 0, max = 200, message = "扩展字段2输入不正确")
    @ApiModelProperty(name = "extField2", value="扩展字段2", example = "str", notes="最大长度：200")
    private String extField2;
    /**
     * 扩展字段3
     */
    @Size(min = 0, max = 200, message = "扩展字段3输入不正确")
    @ApiModelProperty(name = "extField3", value="扩展字段3", example = "str", notes="最大长度：200")
    private String extField3;
    /**
     * 扩展字段4
     */
    @Size(min = 0, max = 200, message = "扩展字段4输入不正确")
    @ApiModelProperty(name = "extField4", value="扩展字段4", example = "str", notes="最大长度：200")
    private String extField4;
    /**
     * 扩展字段5
     */
    @Size(min = 0, max = 200, message = "扩展字段5输入不正确")
    @ApiModelProperty(name = "extField5", value="扩展字段5", example = "str", notes="最大长度：200")
    private String extField5;
    /**
     * 原销售订单号
     */
    @Size(min = 0, max = 20, message = "原销售订单号输入不正确")
    @ApiModelProperty(name = "sourceOrderCode", value="原销售订单号", example = "str", notes="最大长度：20")
    private String sourceOrderCode;
    /**
     * 关联的售后单号
     */
    @Size(min = 0, max = 30, message = "关联的售后单号输入不正确")
    @ApiModelProperty(name = "sourceReturnCode", value="关联的售后单号", example = "str", notes="最大长度：30")
    private String sourceReturnCode;
    /**
     * 服务码
     */
    @Size(min = 0, max = 20, message = "服务码输入不正确")
    @ApiModelProperty(name = "serviceCode", value="服务码", example = "str", notes="最大长度：20")
    private String serviceCode;
    /**
     * 服务日期时间
     */
    @Size(min = 0, max = 1073741824, message = "服务日期时间输入不正确")
    @ApiModelProperty(name = "serviceDateTime", value="服务日期时间", example = "str", notes="最大长度：1073741824")
    private String serviceDateTime;
    /**
     * 是否可用:默认0否;1是
     */
    @ApiModelProperty(name = "isAvailable", value="是否可用:默认0否;1是", example = "1", notes="最大长度：10")
    private Integer isAvailable;

    @Override
    public void setId(Long id) {
        this.id = id;
    }
    @Override
    public Long getId() {
        return id;
    }
    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }
    public String getOrderCode() {
        return orderCode;
    }
    public void setCopyOrderCode(String copyOrderCode) {
        this.copyOrderCode = copyOrderCode;
    }
    public String getCopyOrderCode() {
        return copyOrderCode;
    }
    public void setParentOrderCode(String parentOrderCode) {
        this.parentOrderCode = parentOrderCode;
    }
    public String getParentOrderCode() {
        return parentOrderCode;
    }
    public void setIsLeaf(Integer isLeaf) {
        this.isLeaf = isLeaf;
    }
    public Integer getIsLeaf() {
        return isLeaf;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public Long getUserId() {
        return userId;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getUserName() {
        return userName;
    }
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }
    public Long getMerchantId() {
        return merchantId;
    }
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }
    public Long getCustomerId() {
        return customerId;
    }
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    public String getCustomerName() {
        return customerName;
    }
    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }
    public String getCustomerType() {
        return customerType;
    }
    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }
    public BigDecimal getOrderAmount() {
        return orderAmount;
    }
    public void setProductAmount(BigDecimal productAmount) {
        this.productAmount = productAmount;
    }
    public BigDecimal getProductAmount() {
        return productAmount;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    public String getCurrency() {
        return currency;
    }
    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }
    public String getCurrencyName() {
        return currencyName;
    }
    public void setCurrencyRate(BigDecimal currencyRate) {
        this.currencyRate = currencyRate;
    }
    public BigDecimal getCurrencyRate() {
        return currencyRate;
    }
    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }
    public String getCurrencySymbol() {
        return currencySymbol;
    }
    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }
    public BigDecimal getTaxAmount() {
        return taxAmount;
    }
    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }
    public Integer getOrderStatus() {
        return orderStatus;
    }
    public void setOrderCreateTime(Date orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }
    public Date getOrderCreateTime() {
        return orderCreateTime;
    }
    public void setOrderPaymentType(Integer orderPaymentType) {
        this.orderPaymentType = orderPaymentType;
    }
    public Integer getOrderPaymentType() {
        return orderPaymentType;
    }
    public void setOrderPaymentStatus(Integer orderPaymentStatus) {
        this.orderPaymentStatus = orderPaymentStatus;
    }
    public Integer getOrderPaymentStatus() {
        return orderPaymentStatus;
    }
    public void setOrderPaymentConfirmDate(Date orderPaymentConfirmDate) {
        this.orderPaymentConfirmDate = orderPaymentConfirmDate;
    }
    public Date getOrderPaymentConfirmDate() {
        return orderPaymentConfirmDate;
    }
    public void setOrderDeliveryFee(BigDecimal orderDeliveryFee) {
        this.orderDeliveryFee = orderDeliveryFee;
    }
    public BigDecimal getOrderDeliveryFee() {
        return orderDeliveryFee;
    }
    public void setOrderPaidByCoupon(BigDecimal orderPaidByCoupon) {
        this.orderPaidByCoupon = orderPaidByCoupon;
    }
    public BigDecimal getOrderPaidByCoupon() {
        return orderPaidByCoupon;
    }
    public void setOrderPromotionDiscount(BigDecimal orderPromotionDiscount) {
        this.orderPromotionDiscount = orderPromotionDiscount;
    }
    public BigDecimal getOrderPromotionDiscount() {
        return orderPromotionDiscount;
    }
    public void setOrderGivePoints(BigDecimal orderGivePoints) {
        this.orderGivePoints = orderGivePoints;
    }
    public BigDecimal getOrderGivePoints() {
        return orderGivePoints;
    }
    public void setOrderCancelReasonId(Integer orderCancelReasonId) {
        this.orderCancelReasonId = orderCancelReasonId;
    }
    public Integer getOrderCancelReasonId() {
        return orderCancelReasonId;
    }
    public void setOrderCancelDate(Date orderCancelDate) {
        this.orderCancelDate = orderCancelDate;
    }
    public Date getOrderCancelDate() {
        return orderCancelDate;
    }
    public void setOrderCsCancelReason(String orderCsCancelReason) {
        this.orderCsCancelReason = orderCsCancelReason;
    }
    public String getOrderCsCancelReason() {
        return orderCsCancelReason;
    }
    public void setOrderCanceOperateType(Integer orderCanceOperateType) {
        this.orderCanceOperateType = orderCanceOperateType;
    }
    public Integer getOrderCanceOperateType() {
        return orderCanceOperateType;
    }
    public void setOrderCanceOperateId(String orderCanceOperateId) {
        this.orderCanceOperateId = orderCanceOperateId;
    }
    public String getOrderCanceOperateId() {
        return orderCanceOperateId;
    }
    public void setOrderDeliveryMethodId(String orderDeliveryMethodId) {
        this.orderDeliveryMethodId = orderDeliveryMethodId;
    }
    public String getOrderDeliveryMethodId() {
        return orderDeliveryMethodId;
    }
    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }
    public Long getSalesmanId() {
        return salesmanId;
    }
    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }
    public String getSalesmanName() {
        return salesmanName;
    }
    public void setOrderRemarkUser(String orderRemarkUser) {
        this.orderRemarkUser = orderRemarkUser;
    }
    public String getOrderRemarkUser() {
        return orderRemarkUser;
    }
    public void setOrderRemarkMerchant2user(String orderRemarkMerchant2user) {
        this.orderRemarkMerchant2user = orderRemarkMerchant2user;
    }
    public String getOrderRemarkMerchant2user() {
        return orderRemarkMerchant2user;
    }
    public void setOrderRemarkMerchant(String orderRemarkMerchant) {
        this.orderRemarkMerchant = orderRemarkMerchant;
    }
    public String getOrderRemarkMerchant() {
        return orderRemarkMerchant;
    }
    public void setOrderSource(Integer orderSource) {
        this.orderSource = orderSource;
    }
    public Integer getOrderSource() {
        return orderSource;
    }
    public void setOrderChannel(Integer orderChannel) {
        this.orderChannel = orderChannel;
    }
    public Integer getOrderChannel() {
        return orderChannel;
    }
    public void setOrderPromotionStatus(Integer orderPromotionStatus) {
        this.orderPromotionStatus = orderPromotionStatus;
    }
    public Integer getOrderPromotionStatus() {
        return orderPromotionStatus;
    }
    public void setGoodReceiverAddress(String goodReceiverAddress) {
        this.goodReceiverAddress = goodReceiverAddress;
    }
    public String getGoodReceiverAddress() {
        return goodReceiverAddress;
    }
    public void setGoodReceiverPostcode(String goodReceiverPostcode) {
        this.goodReceiverPostcode = goodReceiverPostcode;
    }
    public String getGoodReceiverPostcode() {
        return goodReceiverPostcode;
    }
    public void setGoodReceiverName(String goodReceiverName) {
        this.goodReceiverName = goodReceiverName;
    }
    public String getGoodReceiverName() {
        return goodReceiverName;
    }
    public void setGoodReceiverMobile(String goodReceiverMobile) {
        this.goodReceiverMobile = goodReceiverMobile;
    }
    public String getGoodReceiverMobile() {
        return goodReceiverMobile;
    }
    public void setGoodReceiverCountry(String goodReceiverCountry) {
        this.goodReceiverCountry = goodReceiverCountry;
    }
    public String getGoodReceiverCountry() {
        return goodReceiverCountry;
    }
    public void setGoodReceiverProvince(String goodReceiverProvince) {
        this.goodReceiverProvince = goodReceiverProvince;
    }
    public String getGoodReceiverProvince() {
        return goodReceiverProvince;
    }
    public void setGoodReceiverCity(String goodReceiverCity) {
        this.goodReceiverCity = goodReceiverCity;
    }
    public String getGoodReceiverCity() {
        return goodReceiverCity;
    }
    public void setGoodReceiverCounty(String goodReceiverCounty) {
        this.goodReceiverCounty = goodReceiverCounty;
    }
    public String getGoodReceiverCounty() {
        return goodReceiverCounty;
    }
    public void setGoodReceiverArea(String goodReceiverArea) {
        this.goodReceiverArea = goodReceiverArea;
    }
    public String getGoodReceiverArea() {
        return goodReceiverArea;
    }
    public void setIdentityCardNumber(String identityCardNumber) {
        this.identityCardNumber = identityCardNumber;
    }
    public String getIdentityCardNumber() {
        return identityCardNumber;
    }
    public void setOrderLogisticsTime(Date orderLogisticsTime) {
        this.orderLogisticsTime = orderLogisticsTime;
    }
    public Date getOrderLogisticsTime() {
        return orderLogisticsTime;
    }
    public void setOrderReceiveDate(Date orderReceiveDate) {
        this.orderReceiveDate = orderReceiveDate;
    }
    public Date getOrderReceiveDate() {
        return orderReceiveDate;
    }
    public void setOrderDeleteStatus(Integer orderDeleteStatus) {
        this.orderDeleteStatus = orderDeleteStatus;
    }
    public Integer getOrderDeleteStatus() {
        return orderDeleteStatus;
    }
    public void setOrderBeforeAmount(BigDecimal orderBeforeAmount) {
        this.orderBeforeAmount = orderBeforeAmount;
    }
    public BigDecimal getOrderBeforeAmount() {
        return orderBeforeAmount;
    }
    public void setOrderBeforeDeliveryFee(BigDecimal orderBeforeDeliveryFee) {
        this.orderBeforeDeliveryFee = orderBeforeDeliveryFee;
    }
    public BigDecimal getOrderBeforeDeliveryFee() {
        return orderBeforeDeliveryFee;
    }
    public void setSysSource(String sysSource) {
        this.sysSource = sysSource;
    }
    public String getSysSource() {
        return sysSource;
    }
    public void setOutOrderCode(String outOrderCode) {
        this.outOrderCode = outOrderCode;
    }
    public String getOutOrderCode() {
        return outOrderCode;
    }
    public void setCommentStatus(Integer commentStatus) {
        this.commentStatus = commentStatus;
    }
    public Integer getCommentStatus() {
        return commentStatus;
    }
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }
    public String getMerchantName() {
        return merchantName;
    }
    public void setOrderRemarkCompany(String orderRemarkCompany) {
        this.orderRemarkCompany = orderRemarkCompany;
    }
    public String getOrderRemarkCompany() {
        return orderRemarkCompany;
    }
    public void setOrderCompleteDate(Date orderCompleteDate) {
        this.orderCompleteDate = orderCompleteDate;
    }
    public Date getOrderCompleteDate() {
        return orderCompleteDate;
    }
    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }
    public Integer getOrderType() {
        return orderType;
    }
    public void setGoodReceiverCountryCode(String goodReceiverCountryCode) {
        this.goodReceiverCountryCode = goodReceiverCountryCode;
    }
    public String getGoodReceiverCountryCode() {
        return goodReceiverCountryCode;
    }
    public void setGoodReceiverProvinceCode(String goodReceiverProvinceCode) {
        this.goodReceiverProvinceCode = goodReceiverProvinceCode;
    }
    public String getGoodReceiverProvinceCode() {
        return goodReceiverProvinceCode;
    }
    public void setGoodReceiverCityCode(String goodReceiverCityCode) {
        this.goodReceiverCityCode = goodReceiverCityCode;
    }
    public String getGoodReceiverCityCode() {
        return goodReceiverCityCode;
    }
    public void setGoodReceiverAreaCode(String goodReceiverAreaCode) {
        this.goodReceiverAreaCode = goodReceiverAreaCode;
    }
    public String getGoodReceiverAreaCode() {
        return goodReceiverAreaCode;
    }
    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }
    public Long getStoreId() {
        return storeId;
    }
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }
    public String getStoreName() {
        return storeName;
    }
    public void setOrderLabel(String orderLabel) {
        this.orderLabel = orderLabel;
    }
    public String getOrderLabel() {
        return orderLabel;
    }
    public void setExpectDeliverDate(Date expectDeliverDate) {
        this.expectDeliverDate = expectDeliverDate;
    }
    public Date getExpectDeliverDate() {
        return expectDeliverDate;
    }
    public void setCashier(String cashier) {
        this.cashier = cashier;
    }
    public String getCashier() {
        return cashier;
    }
    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }
    public String getSourceCode() {
        return sourceCode;
    }
    public void setMealType(Integer mealType) {
        this.mealType = mealType;
    }
    public Integer getMealType() {
        return mealType;
    }
    public void setEquipCode(String equipCode) {
        this.equipCode = equipCode;
    }
    public String getEquipCode() {
        return equipCode;
    }
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
    public String getTableName() {
        return tableName;
    }
    public void setMealsNum(Integer mealsNum) {
        this.mealsNum = mealsNum;
    }
    public Integer getMealsNum() {
        return mealsNum;
    }
    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }
    public String getSeqNo() {
        return seqNo;
    }
    public void setOutSendCode(String outSendCode) {
        this.outSendCode = outSendCode;
    }
    public String getOutSendCode() {
        return outSendCode;
    }
    public void setExtInfo(String extInfo) {
        this.extInfo = extInfo;
    }
    public String getExtInfo() {
        return extInfo;
    }
    public void setExtField1(String extField1) {
        this.extField1 = extField1;
    }
    public String getExtField1() {
        return extField1;
    }
    public void setExtField2(String extField2) {
        this.extField2 = extField2;
    }
    public String getExtField2() {
        return extField2;
    }
    public void setExtField3(String extField3) {
        this.extField3 = extField3;
    }
    public String getExtField3() {
        return extField3;
    }
    public void setExtField4(String extField4) {
        this.extField4 = extField4;
    }
    public String getExtField4() {
        return extField4;
    }
    public void setExtField5(String extField5) {
        this.extField5 = extField5;
    }
    public String getExtField5() {
        return extField5;
    }
    public void setSourceOrderCode(String sourceOrderCode) {
        this.sourceOrderCode = sourceOrderCode;
    }
    public String getSourceOrderCode() {
        return sourceOrderCode;
    }
    public void setSourceReturnCode(String sourceReturnCode) {
        this.sourceReturnCode = sourceReturnCode;
    }
    public String getSourceReturnCode() {
        return sourceReturnCode;
    }
    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }
    public String getServiceCode() {
        return serviceCode;
    }
    public void setServiceDateTime(String serviceDateTime) {
        this.serviceDateTime = serviceDateTime;
    }
    public String getServiceDateTime() {
        return serviceDateTime;
    }
    public void setIsAvailable(Integer isAvailable) {
        this.isAvailable = isAvailable;
    }
    public Integer getIsAvailable() {
        return isAvailable;
    }

	/*---------------------------------- extra info --------------------------------*/
}