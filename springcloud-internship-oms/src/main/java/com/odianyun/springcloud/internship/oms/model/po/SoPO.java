package com.odianyun.springcloud.internship.oms.model.po;

import com.odianyun.project.support.base.model.BasePO;

import java.math.BigDecimal;
import java.util.Date;

/**
 * So
 * @CreateDate 2021-08-02
 */
public class SoPO extends BasePO {
    /**
     * 格式：150905xxxxxxxx2657纯数字6位日期+8位数字+1校验位+3位用户id
     */
    private String orderCode;
    /**
     * 格式同orderCode,复制订单时用来标识复制订单来源
     */
    private String copyOrderCode;
    /**
     * 父order_code
     */
    private String parentOrderCode;
    /**
     * 1子单2父单
     */
    private Integer isLeaf;
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 下单用户账号
     */
    private String userName;
    /**
     * 商家ID
     */
    private Long merchantId;
    /**
     * 经销商id
     */
    private Long customerId;
    /**
     * 客户名称
     */
    private String customerName;
    /**
     * 经销商类型
     */
    private String customerType;
    /**
     * 订单金额(不含运费/运费险)
     */
    private BigDecimal orderAmount;
    /**
     * 订单商品总金额
     */
    private BigDecimal productAmount;
    /**
     * 币别码
     */
    private String currency;
    /**
     * 币别名称
     */
    private String currencyName;
    /**
     * 币种汇率
     */
    private BigDecimal currencyRate;
    /**
     * 币种符号
     */
    private String currencySymbol;
    /**
     * 税费
     */
    private BigDecimal taxAmount;
    /**
     * 订单状态,字典ORDER_STATUS
     */
    private Integer orderStatus;
    /**
     * 下单时间
     */
    private Date orderCreateTime;
    /**
     * 支付方式,字典PAY_METHOD
     */
    private Integer orderPaymentType;
    /**
     * 支付状态,字典ORDER_PAYMENT_STATUS
     */
    private Integer orderPaymentStatus;
    /**
     * 支付确认时间
     */
    private Date orderPaymentConfirmDate;
    /**
     * 运费(实收)
     */
    private BigDecimal orderDeliveryFee;
    /**
     * 支付-抵用券支付的金额
     */
    private BigDecimal orderPaidByCoupon;
    /**
     * 订单已优惠金额(满立减)
     */
    private BigDecimal orderPromotionDiscount;
    /**
     * 订单赠送的积分
     */
    private BigDecimal orderGivePoints;
    /**
     * 取消原因ID
     */
    private Integer orderCancelReasonId;
    /**
     * 取消时间
     */
    private Date orderCancelDate;
    /**
     * 订单取消原因
     */
    private String orderCsCancelReason;
    /**
     * 取消操作人类型：0:用户取消1:系统取消2:客服取消
     */
    private Integer orderCanceOperateType;
    /**
     * 取消操作人用户名
     */
    private String orderCanceOperateId;
    /**
     * 配送方式类型
     */
    private String orderDeliveryMethodId;
    /**
     * 销售员id
     */
    private Long salesmanId;
    /**
     * 销售员name
     */
    private String salesmanName;
    /**
     * 订单备注(用户)
     */
    private String orderRemarkUser;
    /**
     * 订单备注(商家给用户看的)
     */
    private String orderRemarkMerchant2user;
    /**
     * 订单备注(商家自己看的)
     */
    private String orderRemarkMerchant;
    /**
     * 订单来源字典ORDER_SOURCE
     */
    private Integer orderSource;
    /**
     * 订单渠道字典ORDER_CHANNEL
     */
    private Integer orderChannel;
    /**
     * 订单促销状态：1001拼团中，1002拼团成功，1003拼团失败，1004参团失败，1006参团成功，1005取消参团；3001待补货，3002、补货中3003补货成功，3004已取消
     */
    private Integer orderPromotionStatus;
    /**
     * 收货人地址
     */
    private String goodReceiverAddress;
    /**
     * 收货人地址邮编
     */
    private String goodReceiverPostcode;
    /**
     * 收货人姓名
     */
    private String goodReceiverName;
    /**
     * 收货人手机
     */
    private String goodReceiverMobile;
    /**
     * 收货人国家
     */
    private String goodReceiverCountry;
    /**
     * 收货人省份
     */
    private String goodReceiverProvince;
    /**
     * 收货人城市
     */
    private String goodReceiverCity;
    /**
     * 收货人地区
     */
    private String goodReceiverCounty;
    /**
     * 收货人四级区域
     */
    private String goodReceiverArea;
    /**
     * 身份证号码
     */
    private String identityCardNumber;
    /**
     * 订单出库时间
     */
    private Date orderLogisticsTime;
    /**
     * 订单收货时间
     */
    private Date orderReceiveDate;
    /**
     * 0：未删除1：回收站-用户可恢复到02：用户完全删除(客服可协助恢复到0或1)
     */
    private Integer orderDeleteStatus;
    /**
     * 改价前订单金额(不含运费/运费险)
     */
    private BigDecimal orderBeforeAmount;
    /**
     * 改价前运费(实收)
     */
    private BigDecimal orderBeforeDeliveryFee;
    /**
     * 订单来源系统
     */
    private String sysSource;
    /**
     * 外部系统订单编号
     */
    private String outOrderCode;
    /**
     * 评论状态0:未评论1已评论
     */
    private Integer commentStatus;
    /**
     * 商家名称
     */
    private String merchantName;
    /**
     * 平台备注
     */
    private String orderRemarkCompany;
    /**
     * 订单完成时间
     */
    private Date orderCompleteDate;
    /**
     * 订单类型，对应so_type
     */
    private Integer orderType;
    /**
     * 收货人国家code
     */
    private String goodReceiverCountryCode;
    /**
     * 收货人省份code
     */
    private String goodReceiverProvinceCode;
    /**
     * 收货人城市code
     */
    private String goodReceiverCityCode;
    /**
     * 收货人四级区域code
     */
    private String goodReceiverAreaCode;
    /**
     * 店铺Id
     */
    private Long storeId;
    /**
     * 店铺名称
     */
    private String storeName;
    /**
     * 订单标签
     */
    private String orderLabel;
    /**
     * 预计发货日期
     */
    private Date expectDeliverDate;
    /**
     * 收银员-如果是POS渠道的订单，会同步
     */
    private String cashier;
    /**
     * 团单号
     */
    private String sourceCode;
    /**
     * 就餐类型1堂吃2外带
     */
    private Integer mealType;
    /**
     * 订单设备号
     */
    private String equipCode;
    /**
     * 桌名
     */
    private String tableName;
    /**
     * 就餐人数
     */
    private Integer mealsNum;
    /**
     * 订单序号
     */
    private String seqNo;
    /**
     * 外部出库单号
     */
    private String outSendCode;
    /**
     * 扩展信息，以json形式存储
     */
    private String extInfo;
    /**
     * 扩展字段1
     */
    private String extField1;
    /**
     * 扩展字段2
     */
    private String extField2;
    /**
     * 扩展字段3
     */
    private String extField3;
    /**
     * 扩展字段4
     */
    private String extField4;
    /**
     * 扩展字段5
     */
    private String extField5;
    /**
     * 原销售订单号
     */
    private String sourceOrderCode;
    /**
     * 关联的售后单号
     */
    private String sourceReturnCode;
    /**
     * 服务码
     */
    private String serviceCode;
    /**
     * 服务日期时间
     */
    private String serviceDateTime;
    /**
     * 是否可用:默认0否;1是
     */
    private Integer isAvailable;

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }
    public String getOrderCode() {
        return orderCode;
    }
    public void setCopyOrderCode(String copyOrderCode) {
        this.copyOrderCode = copyOrderCode;
    }
    public String getCopyOrderCode() {
        return copyOrderCode;
    }
    public void setParentOrderCode(String parentOrderCode) {
        this.parentOrderCode = parentOrderCode;
    }
    public String getParentOrderCode() {
        return parentOrderCode;
    }
    public void setIsLeaf(Integer isLeaf) {
        this.isLeaf = isLeaf;
    }
    public Integer getIsLeaf() {
        return isLeaf;
    }
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    public Long getUserId() {
        return userId;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getUserName() {
        return userName;
    }
    public void setMerchantId(Long merchantId) {
        this.merchantId = merchantId;
    }
    public Long getMerchantId() {
        return merchantId;
    }
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }
    public Long getCustomerId() {
        return customerId;
    }
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    public String getCustomerName() {
        return customerName;
    }
    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }
    public String getCustomerType() {
        return customerType;
    }
    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }
    public BigDecimal getOrderAmount() {
        return orderAmount;
    }
    public void setProductAmount(BigDecimal productAmount) {
        this.productAmount = productAmount;
    }
    public BigDecimal getProductAmount() {
        return productAmount;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    public String getCurrency() {
        return currency;
    }
    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }
    public String getCurrencyName() {
        return currencyName;
    }
    public void setCurrencyRate(BigDecimal currencyRate) {
        this.currencyRate = currencyRate;
    }
    public BigDecimal getCurrencyRate() {
        return currencyRate;
    }
    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }
    public String getCurrencySymbol() {
        return currencySymbol;
    }
    public void setTaxAmount(BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }
    public BigDecimal getTaxAmount() {
        return taxAmount;
    }
    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }
    public Integer getOrderStatus() {
        return orderStatus;
    }
    public void setOrderCreateTime(Date orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }
    public Date getOrderCreateTime() {
        return orderCreateTime;
    }
    public void setOrderPaymentType(Integer orderPaymentType) {
        this.orderPaymentType = orderPaymentType;
    }
    public Integer getOrderPaymentType() {
        return orderPaymentType;
    }
    public void setOrderPaymentStatus(Integer orderPaymentStatus) {
        this.orderPaymentStatus = orderPaymentStatus;
    }
    public Integer getOrderPaymentStatus() {
        return orderPaymentStatus;
    }
    public void setOrderPaymentConfirmDate(Date orderPaymentConfirmDate) {
        this.orderPaymentConfirmDate = orderPaymentConfirmDate;
    }
    public Date getOrderPaymentConfirmDate() {
        return orderPaymentConfirmDate;
    }
    public void setOrderDeliveryFee(BigDecimal orderDeliveryFee) {
        this.orderDeliveryFee = orderDeliveryFee;
    }
    public BigDecimal getOrderDeliveryFee() {
        return orderDeliveryFee;
    }
    public void setOrderPaidByCoupon(BigDecimal orderPaidByCoupon) {
        this.orderPaidByCoupon = orderPaidByCoupon;
    }
    public BigDecimal getOrderPaidByCoupon() {
        return orderPaidByCoupon;
    }
    public void setOrderPromotionDiscount(BigDecimal orderPromotionDiscount) {
        this.orderPromotionDiscount = orderPromotionDiscount;
    }
    public BigDecimal getOrderPromotionDiscount() {
        return orderPromotionDiscount;
    }
    public void setOrderGivePoints(BigDecimal orderGivePoints) {
        this.orderGivePoints = orderGivePoints;
    }
    public BigDecimal getOrderGivePoints() {
        return orderGivePoints;
    }
    public void setOrderCancelReasonId(Integer orderCancelReasonId) {
        this.orderCancelReasonId = orderCancelReasonId;
    }
    public Integer getOrderCancelReasonId() {
        return orderCancelReasonId;
    }
    public void setOrderCancelDate(Date orderCancelDate) {
        this.orderCancelDate = orderCancelDate;
    }
    public Date getOrderCancelDate() {
        return orderCancelDate;
    }
    public void setOrderCsCancelReason(String orderCsCancelReason) {
        this.orderCsCancelReason = orderCsCancelReason;
    }
    public String getOrderCsCancelReason() {
        return orderCsCancelReason;
    }
    public void setOrderCanceOperateType(Integer orderCanceOperateType) {
        this.orderCanceOperateType = orderCanceOperateType;
    }
    public Integer getOrderCanceOperateType() {
        return orderCanceOperateType;
    }
    public void setOrderCanceOperateId(String orderCanceOperateId) {
        this.orderCanceOperateId = orderCanceOperateId;
    }
    public String getOrderCanceOperateId() {
        return orderCanceOperateId;
    }
    public void setOrderDeliveryMethodId(String orderDeliveryMethodId) {
        this.orderDeliveryMethodId = orderDeliveryMethodId;
    }
    public String getOrderDeliveryMethodId() {
        return orderDeliveryMethodId;
    }
    public void setSalesmanId(Long salesmanId) {
        this.salesmanId = salesmanId;
    }
    public Long getSalesmanId() {
        return salesmanId;
    }
    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }
    public String getSalesmanName() {
        return salesmanName;
    }
    public void setOrderRemarkUser(String orderRemarkUser) {
        this.orderRemarkUser = orderRemarkUser;
    }
    public String getOrderRemarkUser() {
        return orderRemarkUser;
    }
    public void setOrderRemarkMerchant2user(String orderRemarkMerchant2user) {
        this.orderRemarkMerchant2user = orderRemarkMerchant2user;
    }
    public String getOrderRemarkMerchant2user() {
        return orderRemarkMerchant2user;
    }
    public void setOrderRemarkMerchant(String orderRemarkMerchant) {
        this.orderRemarkMerchant = orderRemarkMerchant;
    }
    public String getOrderRemarkMerchant() {
        return orderRemarkMerchant;
    }
    public void setOrderSource(Integer orderSource) {
        this.orderSource = orderSource;
    }
    public Integer getOrderSource() {
        return orderSource;
    }
    public void setOrderChannel(Integer orderChannel) {
        this.orderChannel = orderChannel;
    }
    public Integer getOrderChannel() {
        return orderChannel;
    }
    public void setOrderPromotionStatus(Integer orderPromotionStatus) {
        this.orderPromotionStatus = orderPromotionStatus;
    }
    public Integer getOrderPromotionStatus() {
        return orderPromotionStatus;
    }
    public void setGoodReceiverAddress(String goodReceiverAddress) {
        this.goodReceiverAddress = goodReceiverAddress;
    }
    public String getGoodReceiverAddress() {
        return goodReceiverAddress;
    }
    public void setGoodReceiverPostcode(String goodReceiverPostcode) {
        this.goodReceiverPostcode = goodReceiverPostcode;
    }
    public String getGoodReceiverPostcode() {
        return goodReceiverPostcode;
    }
    public void setGoodReceiverName(String goodReceiverName) {
        this.goodReceiverName = goodReceiverName;
    }
    public String getGoodReceiverName() {
        return goodReceiverName;
    }
    public void setGoodReceiverMobile(String goodReceiverMobile) {
        this.goodReceiverMobile = goodReceiverMobile;
    }
    public String getGoodReceiverMobile() {
        return goodReceiverMobile;
    }
    public void setGoodReceiverCountry(String goodReceiverCountry) {
        this.goodReceiverCountry = goodReceiverCountry;
    }
    public String getGoodReceiverCountry() {
        return goodReceiverCountry;
    }
    public void setGoodReceiverProvince(String goodReceiverProvince) {
        this.goodReceiverProvince = goodReceiverProvince;
    }
    public String getGoodReceiverProvince() {
        return goodReceiverProvince;
    }
    public void setGoodReceiverCity(String goodReceiverCity) {
        this.goodReceiverCity = goodReceiverCity;
    }
    public String getGoodReceiverCity() {
        return goodReceiverCity;
    }
    public void setGoodReceiverCounty(String goodReceiverCounty) {
        this.goodReceiverCounty = goodReceiverCounty;
    }
    public String getGoodReceiverCounty() {
        return goodReceiverCounty;
    }
    public void setGoodReceiverArea(String goodReceiverArea) {
        this.goodReceiverArea = goodReceiverArea;
    }
    public String getGoodReceiverArea() {
        return goodReceiverArea;
    }
    public void setIdentityCardNumber(String identityCardNumber) {
        this.identityCardNumber = identityCardNumber;
    }
    public String getIdentityCardNumber() {
        return identityCardNumber;
    }
    public void setOrderLogisticsTime(Date orderLogisticsTime) {
        this.orderLogisticsTime = orderLogisticsTime;
    }
    public Date getOrderLogisticsTime() {
        return orderLogisticsTime;
    }
    public void setOrderReceiveDate(Date orderReceiveDate) {
        this.orderReceiveDate = orderReceiveDate;
    }
    public Date getOrderReceiveDate() {
        return orderReceiveDate;
    }
    public void setOrderDeleteStatus(Integer orderDeleteStatus) {
        this.orderDeleteStatus = orderDeleteStatus;
    }
    public Integer getOrderDeleteStatus() {
        return orderDeleteStatus;
    }
    public void setOrderBeforeAmount(BigDecimal orderBeforeAmount) {
        this.orderBeforeAmount = orderBeforeAmount;
    }
    public BigDecimal getOrderBeforeAmount() {
        return orderBeforeAmount;
    }
    public void setOrderBeforeDeliveryFee(BigDecimal orderBeforeDeliveryFee) {
        this.orderBeforeDeliveryFee = orderBeforeDeliveryFee;
    }
    public BigDecimal getOrderBeforeDeliveryFee() {
        return orderBeforeDeliveryFee;
    }
    public void setSysSource(String sysSource) {
        this.sysSource = sysSource;
    }
    public String getSysSource() {
        return sysSource;
    }
    public void setOutOrderCode(String outOrderCode) {
        this.outOrderCode = outOrderCode;
    }
    public String getOutOrderCode() {
        return outOrderCode;
    }
    public void setCommentStatus(Integer commentStatus) {
        this.commentStatus = commentStatus;
    }
    public Integer getCommentStatus() {
        return commentStatus;
    }
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }
    public String getMerchantName() {
        return merchantName;
    }
    public void setOrderRemarkCompany(String orderRemarkCompany) {
        this.orderRemarkCompany = orderRemarkCompany;
    }
    public String getOrderRemarkCompany() {
        return orderRemarkCompany;
    }
    public void setOrderCompleteDate(Date orderCompleteDate) {
        this.orderCompleteDate = orderCompleteDate;
    }
    public Date getOrderCompleteDate() {
        return orderCompleteDate;
    }
    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }
    public Integer getOrderType() {
        return orderType;
    }
    public void setGoodReceiverCountryCode(String goodReceiverCountryCode) {
        this.goodReceiverCountryCode = goodReceiverCountryCode;
    }
    public String getGoodReceiverCountryCode() {
        return goodReceiverCountryCode;
    }
    public void setGoodReceiverProvinceCode(String goodReceiverProvinceCode) {
        this.goodReceiverProvinceCode = goodReceiverProvinceCode;
    }
    public String getGoodReceiverProvinceCode() {
        return goodReceiverProvinceCode;
    }
    public void setGoodReceiverCityCode(String goodReceiverCityCode) {
        this.goodReceiverCityCode = goodReceiverCityCode;
    }
    public String getGoodReceiverCityCode() {
        return goodReceiverCityCode;
    }
    public void setGoodReceiverAreaCode(String goodReceiverAreaCode) {
        this.goodReceiverAreaCode = goodReceiverAreaCode;
    }
    public String getGoodReceiverAreaCode() {
        return goodReceiverAreaCode;
    }
    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }
    public Long getStoreId() {
        return storeId;
    }
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }
    public String getStoreName() {
        return storeName;
    }
    public void setOrderLabel(String orderLabel) {
        this.orderLabel = orderLabel;
    }
    public String getOrderLabel() {
        return orderLabel;
    }
    public void setExpectDeliverDate(Date expectDeliverDate) {
        this.expectDeliverDate = expectDeliverDate;
    }
    public Date getExpectDeliverDate() {
        return expectDeliverDate;
    }
    public void setCashier(String cashier) {
        this.cashier = cashier;
    }
    public String getCashier() {
        return cashier;
    }
    public void setSourceCode(String sourceCode) {
        this.sourceCode = sourceCode;
    }
    public String getSourceCode() {
        return sourceCode;
    }
    public void setMealType(Integer mealType) {
        this.mealType = mealType;
    }
    public Integer getMealType() {
        return mealType;
    }
    public void setEquipCode(String equipCode) {
        this.equipCode = equipCode;
    }
    public String getEquipCode() {
        return equipCode;
    }
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }
    public String getTableName() {
        return tableName;
    }
    public void setMealsNum(Integer mealsNum) {
        this.mealsNum = mealsNum;
    }
    public Integer getMealsNum() {
        return mealsNum;
    }
    public void setSeqNo(String seqNo) {
        this.seqNo = seqNo;
    }
    public String getSeqNo() {
        return seqNo;
    }
    public void setOutSendCode(String outSendCode) {
        this.outSendCode = outSendCode;
    }
    public String getOutSendCode() {
        return outSendCode;
    }
    public void setExtInfo(String extInfo) {
        this.extInfo = extInfo;
    }
    public String getExtInfo() {
        return extInfo;
    }
    public void setExtField1(String extField1) {
        this.extField1 = extField1;
    }
    public String getExtField1() {
        return extField1;
    }
    public void setExtField2(String extField2) {
        this.extField2 = extField2;
    }
    public String getExtField2() {
        return extField2;
    }
    public void setExtField3(String extField3) {
        this.extField3 = extField3;
    }
    public String getExtField3() {
        return extField3;
    }
    public void setExtField4(String extField4) {
        this.extField4 = extField4;
    }
    public String getExtField4() {
        return extField4;
    }
    public void setExtField5(String extField5) {
        this.extField5 = extField5;
    }
    public String getExtField5() {
        return extField5;
    }
    public void setSourceOrderCode(String sourceOrderCode) {
        this.sourceOrderCode = sourceOrderCode;
    }
    public String getSourceOrderCode() {
        return sourceOrderCode;
    }
    public void setSourceReturnCode(String sourceReturnCode) {
        this.sourceReturnCode = sourceReturnCode;
    }
    public String getSourceReturnCode() {
        return sourceReturnCode;
    }
    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }
    public String getServiceCode() {
        return serviceCode;
    }
    public void setServiceDateTime(String serviceDateTime) {
        this.serviceDateTime = serviceDateTime;
    }
    public String getServiceDateTime() {
        return serviceDateTime;
    }
    public void setIsAvailable(Integer isAvailable) {
        this.isAvailable = isAvailable;
    }
    public Integer getIsAvailable() {
        return isAvailable;
    }
}