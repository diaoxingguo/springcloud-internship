package com.odianyun.springcloud.internship.oms.service;

import com.odianyun.project.base.IBaseService;
import com.odianyun.project.base.IEntity;
import com.odianyun.project.query.PageQueryArgs;
import com.odianyun.project.query.QueryArgs;
import com.odianyun.springcloud.internship.oms.model.po.SoPO;
import com.odianyun.springcloud.internship.oms.model.vo.SoVO;

public interface SoService extends IBaseService<Long, SoPO, IEntity, SoVO, PageQueryArgs, QueryArgs> {

}
