package com.odianyun.springcloud.internship.oms.service.impl;


import com.odianyun.db.query.PageVO;
import com.odianyun.project.base.IEntity;
import com.odianyun.project.exception.VisibleException;
import com.odianyun.project.model.vo.ObjectResult;
import com.odianyun.project.query.PageQueryArgs;
import com.odianyun.project.query.QueryArgs;
import com.odianyun.project.support.base.service.OdyEntityService;
import com.odianyun.springcloud.internship.common.constant.CommonConstant;
import com.odianyun.springcloud.internship.common.response.model.StoreOrgInfoResponse;
import com.odianyun.springcloud.internship.oms.mapper.SoMapper;
import com.odianyun.springcloud.internship.oms.model.po.SoPO;
import com.odianyun.springcloud.internship.oms.model.vo.SoVO;
import com.odianyun.springcloud.internship.oms.service.SoService;
import com.odianyun.springcloud.internship.oms.soa.client.StoreOrgInfoSoaClient;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Objects;

@Service
public class SoServiceImpl extends OdyEntityService<SoPO, SoVO, PageQueryArgs, QueryArgs, SoMapper> implements SoService {
    @Resource
    private SoMapper mapper;

    @Resource
    private StoreOrgInfoSoaClient storeOrgInfoSoaClient;

    @Override
    protected SoMapper getMapper() {
        return mapper;
    }

    @Override
	protected void afterListPage(PageVO<? extends IEntity> pageVO) {
	    for (IEntity entity : pageVO.getList()) {
	        SoVO vo = (SoVO) entity;
            vo.setOrderCode("orderCode");
	    }
	}


    @Override
    public SoVO getById(Long aLong) {
        SoVO soVO = super.getById(aLong);
        if(null == soVO || null == soVO.getStoreId()) {
            throw new VisibleException("数据不存在");
        }
        // 获取店铺信息
        ObjectResult<StoreOrgInfoResponse> response = storeOrgInfoSoaClient.getByOrgId(soVO.getStoreId());
        if (Objects.equals(CommonConstant.SUCCESS_CODE, response.getCode())
        && null != response.getData()) {
            StoreOrgInfoResponse storeOrgInfoResponse =  response.getData();
            soVO.setStoreProvinceName(storeOrgInfoResponse.getProvinceName());
            soVO.setStoreCityName(storeOrgInfoResponse.getCityName());
            soVO.setStoreRegionName(storeOrgInfoResponse.getRegionName());
            soVO.setStoreDetailName(storeOrgInfoResponse.getDetailAddress());
        }

        return soVO;
    }
}
