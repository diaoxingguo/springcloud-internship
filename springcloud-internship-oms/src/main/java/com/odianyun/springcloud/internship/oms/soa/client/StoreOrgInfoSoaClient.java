package com.odianyun.springcloud.internship.oms.soa.client;

import com.odianyun.project.model.vo.ObjectResult;
import com.odianyun.springcloud.internship.common.response.model.StoreOrgInfoResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @description:
 * @author: EDZ
 * @time: 10:26
 * @date: 2021/8/3
 */
@FeignClient(value = "springcloud-internship-ouser-soa", path = "springcloud-internship-ouser/storeOrgInfoSoaService")
public interface StoreOrgInfoSoaClient {
    @GetMapping("getByOrgId")
    ObjectResult<StoreOrgInfoResponse> getByOrgId(@RequestParam("orgId") Long orgId);
}
