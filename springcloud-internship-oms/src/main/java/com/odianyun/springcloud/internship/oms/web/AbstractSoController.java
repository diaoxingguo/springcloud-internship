package com.odianyun.springcloud.internship.oms.web;

import com.odianyun.db.query.PageVO;
import com.odianyun.project.model.vo.ObjectResult;
import com.odianyun.project.model.vo.PageResult;
import com.odianyun.project.model.vo.Result;
import com.odianyun.project.query.PageQueryArgs;
import com.odianyun.project.support.base.controller.BaseController;
import com.odianyun.springcloud.internship.oms.model.dto.SoDTO;
import com.odianyun.springcloud.internship.oms.model.vo.SoVO;
import com.odianyun.springcloud.internship.oms.service.SoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

@Api(value="SoController", tags="So")
@RequestMapping("so")
@RestController
public abstract class AbstractSoController<DTO extends SoDTO> extends BaseController {
    @Resource
    protected SoService service;

	@ApiOperation(value="So-分页查询")
	@PostMapping("/listPage")
    public PageResult<SoVO> listPage(@RequestBody @ApiParam(value="查询入参, JSON示例为{page:0,limit:0,filters:{name:\"test\"},sorts:[{field:\"createTime\",asc:false}]}") PageQueryArgs args) {
        PageVO<SoVO> pageVO = service.listPage(args);
        return PageResult.ok(pageVO);
    }

    @ApiOperation("So-查询")
    @GetMapping("/getById")
    public ObjectResult<SoVO> getById(@RequestParam("id") Long id) {
        SoVO vo = service.getById(id);

        return ObjectResult.ok(vo);
    }

    @ApiOperation("So-添加")
    @PostMapping("/add")
    public ObjectResult<Long> add(@RequestBody @Valid DTO u) throws Exception {
        notNull(u);

        return ObjectResult.ok(service.addWithTx(u));
    }

    @ApiOperation("So-修改")
    @PostMapping("/update")
    public Result update(@RequestBody @Valid DTO u) throws Exception {
        notNull(u);
        fieldNotNull(u, "id");

        service.updateWithTx(u);
        return Result.OK;
    }

    @ApiOperation("So-删除")
    @PostMapping("/delete")
    public Result delete(@RequestBody Long[] ids) throws Exception {
        notNull(ids);

        service.deletesWithTx(ids);
        return Result.OK;
    }
	
}
