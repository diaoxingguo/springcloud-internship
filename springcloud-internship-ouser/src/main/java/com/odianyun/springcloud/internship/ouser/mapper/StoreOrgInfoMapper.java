package com.odianyun.springcloud.internship.ouser.mapper;

import com.odianyun.db.mybatis.BaseJdbcMapper;
import com.odianyun.springcloud.internship.ouser.model.po.StoreOrgInfoPO;

public interface StoreOrgInfoMapper extends BaseJdbcMapper<StoreOrgInfoPO, Long> {

}