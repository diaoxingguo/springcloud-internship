package com.odianyun.springcloud.internship.ouser.service;

import com.odianyun.project.base.IBaseService;
import com.odianyun.project.base.IEntity;
import com.odianyun.project.query.PageQueryArgs;
import com.odianyun.project.query.QueryArgs;
import com.odianyun.springcloud.internship.ouser.model.po.StoreOrgInfoPO;
import com.odianyun.springcloud.internship.ouser.model.vo.StoreOrgInfoVO;

public interface StoreOrgInfoService extends IBaseService<Long, StoreOrgInfoPO, IEntity, StoreOrgInfoVO, PageQueryArgs, QueryArgs> {

    StoreOrgInfoVO getByOrgId(Long orgId);
}
