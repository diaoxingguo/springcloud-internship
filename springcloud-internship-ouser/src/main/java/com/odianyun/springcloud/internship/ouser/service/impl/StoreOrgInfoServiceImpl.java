package com.odianyun.springcloud.internship.ouser.service.impl;

import com.odianyun.db.query.PageVO;
import com.odianyun.project.base.IEntity;
import com.odianyun.project.query.PageQueryArgs;
import com.odianyun.project.query.QueryArgs;
import com.odianyun.project.support.base.db.Q;
import com.odianyun.project.support.base.service.OdyEntityService;
import com.odianyun.springcloud.internship.ouser.mapper.StoreOrgInfoMapper;
import com.odianyun.springcloud.internship.ouser.model.po.StoreOrgInfoPO;
import com.odianyun.springcloud.internship.ouser.model.vo.StoreOrgInfoVO;
import com.odianyun.springcloud.internship.ouser.service.StoreOrgInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class StoreOrgInfoServiceImpl extends OdyEntityService<StoreOrgInfoPO, StoreOrgInfoVO, PageQueryArgs, QueryArgs, StoreOrgInfoMapper> implements StoreOrgInfoService {
    @Resource
    private StoreOrgInfoMapper mapper;

    @Override
    protected StoreOrgInfoMapper getMapper() {
        return mapper;
    }

    @Override
	protected void afterListPage(PageVO<? extends IEntity> pageVO) {
	    for (IEntity entity : pageVO.getList()) {
	        StoreOrgInfoVO vo = (StoreOrgInfoVO) entity;
            vo.setIsAvailable(0);
            vo.setVersionNo(0);
	    }
	}

    @Override
    public StoreOrgInfoVO getByOrgId(Long orgId) {
        return super.get(new Q().eq("orgId", orgId));
    }
}
