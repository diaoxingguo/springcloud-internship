package com.odianyun.springcloud.internship.ouser.soa.service;

import com.odianyun.project.exception.VisibleException;
import com.odianyun.project.model.vo.ObjectResult;
import com.odianyun.springcloud.internship.common.response.model.StoreOrgInfoResponse;
import com.odianyun.springcloud.internship.ouser.model.vo.StoreOrgInfoVO;
import com.odianyun.springcloud.internship.ouser.service.StoreOrgInfoService;
import com.odianyun.util.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @description:
 * @author: EDZ
 * @time: 10:11
 * @date: 2021/8/3
 */
@RestController
@RequestMapping("storeOrgInfoSoaService")
public class StoreOrgInfoSoaService {

    @Resource
    private StoreOrgInfoService storeOrgInfoService;

    @GetMapping("getByOrgId")
    public ObjectResult<StoreOrgInfoResponse> getByOrgId(@RequestParam("orgId") Long orgId) {
        StoreOrgInfoVO vo = storeOrgInfoService.getByOrgId(orgId);
        if (null == vo) {
           return ObjectResult.ok(null);
        }
        StoreOrgInfoResponse response = BeanUtils.copyProperties(vo, StoreOrgInfoResponse.class);
        return ObjectResult.ok(response);
    }
}
