package com.odianyun.springcloud.internship.ouser.startup;

import com.github.pagehelper.autoconfigure.PageHelperAutoConfiguration;
import org.mybatis.spring.boot.autoconfigure.MybatisAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @description:
 * @author: EDZ
 * @time: 9:43
 * @date: 2021/7/13
 */
@SpringBootApplication(scanBasePackages = "com.odianyun.springcloud.internship.ouser", exclude = {MybatisAutoConfiguration.class, PageHelperAutoConfiguration.class})
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"com.odianyun.springcloud.internship.ouser"})
public class OuserApplication {
    public static void main(String[] args) {
        SpringApplication.run(OuserApplication.class, args);
    }
}