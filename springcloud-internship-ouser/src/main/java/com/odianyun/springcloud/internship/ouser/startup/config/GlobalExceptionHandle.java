package com.odianyun.springcloud.internship.ouser.startup.config;

import com.odianyun.project.exception.VisibleException;
import com.odianyun.project.message.CodeEnum;
import com.odianyun.project.model.vo.Result;
import com.odianyun.project.util.ValidUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @description:
 * @author: EDZ
 * @time: 14:59
 * @date: 2021/7/28
 */
@RestControllerAdvice
public class GlobalExceptionHandle {

    private final static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandle.class);

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public Object handleValidException(MethodArgumentNotValidException exception) {
        String message = ValidUtils.extractErrorMessage(exception);
        logger.debug("MethodArgumentNotValidException---", exception);
        logger.info("validMsg == {}", message);
        return new Result(CodeEnum.VALIDATE_ERROR.getCode(), message);
    }

    @ExceptionHandler(VisibleException.class)
    public Object handleVisibleException(VisibleException e) {
        logger.debug("VisibleException---", e);
        return new Result(CodeEnum.SERVICE_ERROR.getCode(), e.getMessage());
    }

    @ExceptionHandler(value = Exception.class)
    public Object handleValidException(Exception exception) {
        logger.info("服务异常", exception);
        return new Result(CodeEnum.ERROR.getCode(), "服务飞到外太空去了");
    }
}
