package com.odianyun.springcloud.internship.ouser.startup.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @description:
 * @author: EDZ
 * @time: 10:40
 * @date: 2021/7/26
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket openApi() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .groupName("ouser示例")
                .apiInfo(new ApiInfoBuilder().title("ouser开放接口文档").build())
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.odianyun.springcloud.internship.ouser.web"))
                .paths(PathSelectors.any())
                .build();
        return docket;

    }
}
