package com.odianyun.springcloud.internship.ouser.web;

import com.odianyun.db.query.PageVO;
import com.odianyun.project.model.vo.ObjectResult;
import com.odianyun.project.model.vo.PageResult;
import com.odianyun.project.model.vo.Result;
import com.odianyun.project.query.PageQueryArgs;
import com.odianyun.project.support.base.controller.BaseController;
import com.odianyun.springcloud.internship.ouser.model.dto.StoreOrgInfoDTO;
import com.odianyun.springcloud.internship.ouser.model.vo.StoreOrgInfoVO;
import com.odianyun.springcloud.internship.ouser.service.StoreOrgInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

@Api(value="StoreOrgInfoController", tags="StoreOrgInfo")
@RequestMapping("storeOrgInfo")
@RestController
public abstract class AbstractStoreOrgInfoController<DTO extends StoreOrgInfoDTO> extends BaseController {
    @Resource
    protected StoreOrgInfoService service;

	@ApiOperation(value="StoreOrgInfo-分页查询")
	@PostMapping("/listPage")
    public PageResult<StoreOrgInfoVO> listPage(@RequestBody @ApiParam("查询入参, JSON示例为{page:0,limit:0,filters:{name:\"test\"},sorts:[{field:\"createTime\",asc:false}]}") PageQueryArgs args) {
        PageVO<StoreOrgInfoVO> pageVO = service.listPage(args);
        return PageResult.ok(pageVO);
    }

    @ApiOperation("StoreOrgInfo-查询")
    @GetMapping("/getById")
    public ObjectResult<StoreOrgInfoVO> getById(@RequestParam("id") Long id) {
        StoreOrgInfoVO vo = service.getById(id);

        return ObjectResult.ok(vo);
    }

    @ApiOperation("StoreOrgInfo-添加")
    @PostMapping("/add")
    public ObjectResult<Long> add(@RequestBody @Valid DTO u) throws Exception {
        notNull(u);

        return ObjectResult.ok(service.addWithTx(u));
    }

    @ApiOperation("StoreOrgInfo-修改")
    @PostMapping("/update")
    public Result update(@RequestBody @Valid DTO u) throws Exception {
        notNull(u);
        fieldNotNull(u, "id");

        service.updateWithTx(u);
        return Result.OK;
    }

    @ApiOperation("StoreOrgInfo-删除")
    @PostMapping("/delete")
    public Result delete(@RequestBody Long[] ids) throws Exception {
        notNull(ids);

        service.deletesWithTx(ids);
        return Result.OK;
    }
	
}
