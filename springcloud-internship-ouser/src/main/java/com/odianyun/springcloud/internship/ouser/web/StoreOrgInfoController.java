package com.odianyun.springcloud.internship.ouser.web;

import com.odianyun.springcloud.internship.ouser.model.dto.StoreOrgInfoDTO;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StoreOrgInfoController extends AbstractStoreOrgInfoController<StoreOrgInfoDTO> {
	
}
